export class ApplicationDisplayModel {
  listMenu: MenuItem[];
  constructor() {
    this.listMenu = [];
  }
}

export class MenuItem {
  url: string;
  item: string;
  listSubMenu?: MenuItem[];

  constructor(url?: string, item?: string, listSubMenu?: MenuItem[]) {
    this.url = url;
    this.item = item;
    this.listSubMenu = listSubMenu;
  }
}
