import {Action} from '@ngrx/store';

export const DISPLAY_LOADER = '[Layout] Open LOADER';
export const CLOSE_LOADER = '[Layout] Close LOADER';
export const RESIZE_SCREEN = '[Layout] resize screen';

export class ActionDisplayLoader implements Action {
  readonly type = DISPLAY_LOADER;
}

export class ActionCloseLoader implements Action {
  readonly type = CLOSE_LOADER;
}

export class ActionResizeWindows implements Action {
  readonly type = RESIZE_SCREEN;

  constructor(public payload: number) {
  }
}

export type Actions = ActionDisplayLoader | ActionCloseLoader | ActionResizeWindows;
