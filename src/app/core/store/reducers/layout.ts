import * as layout from '../actions/layout';
import { MenuItem } from '@app/core/models/ApplicationDisplay';

export interface State {
  displayLoader: boolean;
  isDesktop: boolean;
  isMedium: boolean;
  menus: MenuItem[];
}

const initialState: State = {
  displayLoader: false,
  isDesktop: true,
  isMedium: false,
  menus: [{item: 'home', url: 'home'}]
};

export function reducer(state = initialState, action: layout.Actions): State {
  switch (action.type) {
    case layout.CLOSE_LOADER:
      return {...state,
        displayLoader: false,
      };

    case layout.DISPLAY_LOADER:
      return {...state,
        displayLoader: true,
      };
    case layout.RESIZE_SCREEN:
      return {...state, isDesktop:  action.payload > 704, isMedium : action.payload < 992};

    default:
      return {...state};
  }
}
