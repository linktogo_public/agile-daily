import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { ActionType } from '@app/core/models/enum/ActionType';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

export interface Item { id: string; name: string; }

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private messagesCollection: AngularFirestoreCollection<Item>;
  public messages: Observable<Item[]>;

  public actionCreate: ActionType = ActionType.CREATE;
  public actionJoin: ActionType = ActionType.JOIN;
  public action: ActionType;

  public currentForm: FormGroup;

  constructor(private firestore: AngularFirestore,
              translate: TranslateService) {
    translate.use('fr');
}

  ngOnInit(): void {
    this.messagesCollection = this.firestore.collection<Item>('messages');
    this.messages = this.messagesCollection.valueChanges();
    this.createFrorm();
  }

  createFrorm(): void {
    this.currentForm = new FormGroup({
      room: new FormControl('', Validators.required),
    });
  }

  add(name: string){// Persist a document id
    const id = this.firestore.createId();
    name = name + ' : '  + new Date().getTime();
    const item: Item = { id, name };
    this.messagesCollection.doc(id).set(item);
  }

  choose(action: ActionType){
    console.log(action);
    this.action = action;
  }
}
