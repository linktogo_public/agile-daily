import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BreakpointState, Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { MenuItem } from '@app/core/models/ApplicationDisplay';
import { State, selectLayout } from '@app/core/store';
import { Store, select } from '@ngrx/store';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isHandset: Observable<BreakpointState> = this.breakpointObserver.observe(Breakpoints.Handset);
  isOpen = false;
  listMenu: MenuItem[] = [];

  constructor(private breakpointObserver: BreakpointObserver, private store: Store<State>) {
}

ngOnInit(): void {
  this.store.pipe(
    select(selectLayout)
    )
  .subscribe( state => {
    this.listMenu = state.menus;
  });
}

goTo(target: string) {
}

}
