export const environment = {
  production: false,
  firebase: {
    apiKey: 'xxxxxxx',
    authDomain: 'linktogo-agile.firebaseapp.com',
    databaseURL: 'https://linktogo-agile.firebaseio.com',
    projectId: 'linktogo-agile',
    storageBucket: 'linktogo-agile.appspot.com',
    messagingSenderId: 'xxxxxxx',
    appId: 'xxxxxxxxxx',
    measurementId: 'xxxxxx'
  }
};
