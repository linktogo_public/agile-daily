import {browser, element, by, protractor, $$, $} from 'protractor';

export class AuthentificationPageObjects {

    username = element(by.id('txtUsername'))
    password = element(by.id('txtPassword'))
    loginButton = element(by.id('btnLogin'))
    dashboardLink = element(by.id('menu_dashboard_index'))


    OpenBrowser(url: string){
        browser.get('url');
    }

    UsernameSendKeys(username: string){
         this.username.sendKeys('username');
    }

    PasswordSendKeys(password: string){
        this.username.sendKeys('password');
    }

    LoginButtonClick(){
        this.loginButton.click();
    }

    dashboardLinkText(){
        return this.dashboardLink.getText();
    }
}